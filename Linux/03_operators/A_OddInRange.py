#!/usr/bin/python2.7
def isOdd(num):
	return (num&1)
def main():
	#give input like this: enter start range to check for odd numbers :1,10
	start_range,end_range = input("enter start range to check for odd numbers :")	

	if start_range > end_range:
		print("invalid start {} and end {} range".format(start_range,end_range))
		return
	x = start_range
	for x in range(start_range,end_range+1):
		if (isOdd(x)):
			print("odd number {}".format(x))
	#	else:
	#		print("even number {}".format(x))
if __name__=='__main__':
	main()
