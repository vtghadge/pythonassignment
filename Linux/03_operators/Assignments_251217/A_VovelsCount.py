#!/usr/bin/python2.7
def CountVovels(strAlpha):
	strLen = len(strAlpha)
	i = 0
	vovelCnt = 0;
	consonentCnt = 0;
	strAlphaLower = strAlpha.lower()
	for i in range(strLen):
		if (False == strAlphaLower[i].isalpha()):
			continue
		#strAlphaLower[i] in "aeiou"	Optimize solution.
		if (strAlphaLower[i]=='a' or strAlphaLower[i]=='e' or strAlphaLower[i]=='i' or strAlphaLower[i]=='o' or strAlphaLower[i]=='u'):
			vovelCnt = vovelCnt+1
		else:
			consonentCnt = consonentCnt+1
	print("Vovels count = {} and Consonent count = {} in string {}".format(vovelCnt, consonentCnt,strAlpha))
def main():
	strAlpha = input("Enter String to count number of vovels in it:")
	CountVovels(strAlpha)
if __name__=='__main__':
	main()
