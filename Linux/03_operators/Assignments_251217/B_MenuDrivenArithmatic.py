#!/usr/bin/python2.7
def main():
	while(1):
		print("---------------------Menu-----------------------------")
		choice = input("1.Add \n2.Sub \n3.Mult \n4.Division \n0.Exit \nEnter choice  ")
		if (choice == 1):
			num1,num2=input("Enter any 2 numbers to perform arithmatic operations : ")
			print("Addition of {} and {} is {}".format(num1,num2,num1+num2))
		elif(choice == 2):
			num1,num2=input("Enter any 2 numbers to perform arithmatic operations : ")
			print("Substraction of {} and {} is {}".format(num1,num2,num1-num2))
		elif(choice == 3):
			num1,num2=input("Enter any 2 numbers to perform arithmatic operations : ")
			print("Multiplication of {} and {} is {}".format(num1,num2,num1*num2))
		elif(choice == 4):
			if (num2 == 0):
				print("Divide by zero error")
			else:
				num1,num2=input("Enter any 2 numbers to perform arithmatic operations : ")
				print("Division of {} and {} is {}".format(num1,num2,num1/num2))
		elif(choice == 0):
			print("Exiting...")
			return
		else:
			print("\nInvalid choice...Try again")

if __name__=='__main__':
	main()
