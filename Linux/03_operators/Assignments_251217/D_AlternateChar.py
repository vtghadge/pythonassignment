#!/usr/bin/python2.7
def PrintAlternate(inStr):
	strLen = len(inStr)
	strFormatted = inStr[:2]+inStr[(strLen-2):]+inStr[2:(strLen-3):2]+inStr[3:(strLen-3):2]
	print("Original string {} and formatted string {} ".format(inStr, strFormatted))
def main():
	inStr = input("Enter string to print first 2 and last 2 and then print alternate character : ")
	PrintAlternate(inStr)
if __name__=='__main__':
	main()
