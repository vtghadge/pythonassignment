#!/usr/bin/python2.7
def main():
	inStr1,inStr2=input("Enter 2 string and swap first 2 characters: ")
	inStr1Res = inStr2[:2]+inStr1[2:]
	inStr2Res = inStr1[:2]+inStr2[2:]
	print("Resulting strings are first: {} and second: {} ".format(inStr1Res,inStr2Res))
if __name__=="__main__":
	main()
