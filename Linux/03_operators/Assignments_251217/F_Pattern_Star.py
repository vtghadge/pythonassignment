#!/usr/bin/python2.7
def PrintPattern(Row):
	i = 1
	start = 1
	for i in range(start,Row+1):
		j = 1
		for j in range(start,i+1):
			print'*',
		print("\n")
def main():
	# Accept no of rows from user.
	iRow = input("Enter no of rows for pattern: ")
	PrintPattern(iRow)
	
if __name__=='__main__':
	main()
