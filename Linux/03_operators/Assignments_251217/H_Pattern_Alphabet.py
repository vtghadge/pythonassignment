#!/usr/bin/python2.7
def PrintPattern(Row):
	i = 0
	alpha="ABCDEFGHIJKLMNOPQRSTUVWXYZ" 
	ch = 'A'
	for i in range(Row):
		j=0
		for j in range(i+1):
			print alpha[j],
		print("\n")
def main():
	iRow = input("Enter number of rows to print: ")
	if iRow>26:
		print("row excceds alphabet count, Exiting...")
		return
	PrintPattern(iRow)
	
if __name__=='__main__':
	main()
