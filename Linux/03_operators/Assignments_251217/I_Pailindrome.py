#!/usr/bin/python2.7
def IsPalindrome(num):
	if num == 0:
		return False
	revNum = 0
	tempNum = num
	while tempNum > 0:
		revNum = (revNum * 10) + (tempNum%10)
		tempNum = tempNum/10

	return (revNum==num)  
  
def main():
	num = input("Enter number to check palindrome or not: ")
	bPalindrome = IsPalindrome(num)
	strRes="is not"
	if (bPalindrome):
		strRes = "is"
	print("Number {} {} palindrome".format(num,strRes))

if __name__=="__main__":
	main()
