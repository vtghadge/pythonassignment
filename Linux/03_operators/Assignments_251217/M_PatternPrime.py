#!/usr/bin/python27
import B_IsPrime
def GetNextPrime(num):
	bPrime = True
	while bPrime:
		if B_IsPrime.isPrime(num):
			return num
		else:
			num = num + 1
def PatternPrime(rows):
	NextPrime = 1;
	for i in range(rows):
		for j in range(i+1):
			NextPrime = GetNextPrime(NextPrime + 1)
			print NextPrime,
		print
				
def main():
	num = input("Enter no of rows to print prime pattern:")
	PatternPrime(num)
if __name__=="__main__":
	main()
