#!/usr/bin/python27
'''
	* * * * * * * * * 
	* * * *   * * * * 
	* * *       * * * 
	* *           * * 
	*               * 
'''
def PatternFlag(iRow):
	A = 65
	i = 1
	k = 0
	for i in range(iRow):
		for j in range((iRow-i)):
			print "*",
		for k in range((2*i)-1):
			print " ",
		for l in range(iRow-i):
			if l==(iRow-1):
				continue
			print "*",		
		print("")
def main():
	iRow = input("Enter number of rows : ")
	PatternFlag(iRow)
if __name__=="__main__":
	main()
