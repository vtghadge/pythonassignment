#!/usr/bin/python27
def CountOfCharAndWord(inStr):
	strLen = len(inStr)
	iWordCnt = 0
	WordCntFlag = 0 
	iCharCnt = 0
	strTemp = inStr.rstrip().lstrip()
	strLen = len(strTemp)
	if strLen > 0:
		iWordCnt = 1
	for i in range(strLen):
		if (strTemp[i].isspace()):
			if 1 == WordCntFlag:
				iWordCnt = iWordCnt + 1
				WordCntFlag = 0
		elif (strTemp[i].isalpha()):
			iCharCnt = iCharCnt + 1
			WordCntFlag = 1
		else:
			WordCntFlag = 1
	print("String '{}' has Words : {} and Characters : {}".format(inStr,iWordCnt,iCharCnt))
	
def main():
	inputStr = input("Enter string to count characters and words from given string: ")
	CountOfCharAndWord(inputStr)
if __name__=="__main__":
	main()
