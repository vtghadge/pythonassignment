#!/usr/bin/python27
def PrintNotMultipleOf2And3(start,end):
	i = start
	for i in range(end):
		if (i % 2 == 0 or i % 3 == 0):
			continue
		else:
			print(" {}".format(i)),
def main():
	start,end = input("Enter start and end(range) and print numbers which are not multiple of 2 and 3 : ")
	PrintNotMultipleOf2And3(start,end)
	
if __name__=="__main__":
	main()
