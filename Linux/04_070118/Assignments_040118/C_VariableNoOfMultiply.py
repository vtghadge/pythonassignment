#!/usr/bin/python27
def VariableNoOfMultiplication(num1, num2=1, num3=1, num4=1, num5=1):
	return (num1*num2*num3*num4*num5)
def main():
	
	print "Multiplication of 2 and 3 :",VariableNoOfMultiplication(2,3)
	print "Multiplication of 2,3 and 4 :",VariableNoOfMultiplication(2,3,4)
	print "Multiplication of 2,3,4 and 5 :",VariableNoOfMultiplication(2,3,4,5)
	print "Multiplication of 2,3,4,5 And 6 :",VariableNoOfMultiplication(2,3,4,5,6)
if __name__=="__main__":
	main()
