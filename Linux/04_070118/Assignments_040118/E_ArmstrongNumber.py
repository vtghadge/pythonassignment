'''
WAP to accept number from user and check for armstrong number.
	1^3+5^3+3^3 = 153
'''
#!/usr/bin/python27
def Armstrong(num):
	numTemp = num
	addition = 0
	while numTemp > 0:
		addition = addition + (pow((numTemp % 10) ,3))
		numTemp = numTemp / 10
	return (addition == num)
def main():
	num = input("Enter number to check for armstrong number: ")
	strTemp = "not "
	if (Armstrong(num)):
		strTemp=""
	print("Number {} is {}Armstrong number".format(num, strTemp))
if __name__=="__main__":
	main()

