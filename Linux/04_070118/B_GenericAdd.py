def GenericAdd(a,b,c=0,d=0,e=0):
	return a+b+c+d+e
	
def GenericMul(a,b,c=1,d=1,e=1):
	return a*b*c*d*e
	
def main():
	print GenericAdd(10,20)
	print GenericAdd(10,20,30)
	print GenericAdd(10,20,30,40)
	print GenericAdd(10,20,30,40,50)
	
	print GenericMul(2,3)
	print GenericMul(2,3,4)
	print GenericMul(2,3,4,5)
	print GenericMul(2,3,4,5,6)
	
if __name__=='__main__':
	main()
