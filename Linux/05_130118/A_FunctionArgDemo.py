def VariableArgsDictionaryDemo(a,b,c,*args,**kwargs):
	# formal arguments must befor variable argument(*args).
	# variable args(*args) must before dictionary(**kwargs)

	'''
	O/P
	<type 'tuple'>
	4
	5
	9
	10
	<type 'dict'>
	('Hobby', 'Playing cricket')
	('name', 'Vishal')
	'''
	print(a,b,c)
	print (type(args))
	for n in args:
		print n
	print(type(kwargs))
	for key in kwargs:
		print(key,kwargs[key])
	'''one more way to iterate dictionary
	'''
	for key,value in kwargs.items():
		print(key,value)
		
def VariableNumberOfArgDemo(*args):
	print type(args),
	for n in args:
		print n,
def DefaultArgDemo(a,b=100):
	print("Default argument pass are a = {} b = {} ".format(a,b))
def KeywordArgDemo(a,b):
	print("Keyword Argument pass are a = {} b = {} ".format(a,b))
def PositionalArgDemo(a,b):
	print("Positional Argument pass are a = {} b = {} ".format(a,b))
def main():
	PositionalArgDemo(10,20)
	KeywordArgDemo(b=10,a=20)
	DefaultArgDemo(10)
	DefaultArgDemo(10,20)
	VariableNumberOfArgDemo(1,2,3,4)
	VariableNumberOfArgDemo(1,2,3,4,15,"hi","vishal",2.5)
	VariableArgsDictionaryDemo(1,2,3,4,5,9,10,name="Vishal",Hobby="Playing cricket")
	'''
	Positional Argument pass are a = 10 b = 20 
	Keyword Argument pass are a = 20 b = 10 
	Default argument pass are a = 10 b = 100 
	Default argument pass are a = 10 b = 20 
	<type 'tuple'> 1 2 3 4 <type 'tuple'> 1 2 3 4 15 hi vishal 2.5
	'''
if __name__=='__main__':
	main()
	
