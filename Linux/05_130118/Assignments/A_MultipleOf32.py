#!/usr/bin/python27
import C_NumIs2sPower

def IsInMultiple(num,divisor=32):
	return ((num & (divisor - 1)) == 0)

def main():
	num,devisor = input("Enter number and devisor to check if number is multiple of divisor or not: ")
	if (IsIn2sPower(devisor)):
		print("devisor is not in 2s power")
		return
	strRes = "Not "
	if (IsInMultiple(num,devisor)):
		strRes=""
	print("Number {} is {}multiple of 32".format(num,strRes))
	
if __name__=="__main__":
	main()
