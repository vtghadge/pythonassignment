#!/usr/bin/python27
import math
'''def CountOnBits(num):
	if (num < 0):
		maxNum = max(pow(2,31)-1, pow(2,64)-1)
	else:
		maxNum = num
	temp = 1
	count = 0
	while(temp <= maxNum):
		if num & temp:
			count+=1
		temp = temp<<1
	return count
'''
def CountOnBits(num):
	if (num < 0):
		maxNum = max(pow(2,31)-1, pow(2,64)-1)
	else:
		maxNum = num
	temp = 1
	count = 0
	while(temp <= maxNum):
		if num & temp:
			count+=1
		temp = temp<<1
	return count

def main():
	num= input("Enter number and count 1 bits in it: ")
	print("No of 1's in number {} is {} ".format(num,CountOnBits(num)))
if __name__=='__main__':
	main()
