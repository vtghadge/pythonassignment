def IsIn2sPower(num):
	return ((num & (num-1))==0)
def main():
	num = input("Enter number to check if number is 2's power: ")
	strRes="not "
	if (IsIn2sPower(num)):
		strRes=""
	print("Number {} is {}2s power".format(num,strRes))
if __name__=='__main__':
	main()
