def GCD1(num1,num2):
	if (num1==num2):
		return num1
	if (num1>num2):
		return GCD1(num2, num1-num2)
	else:
		return GCD1(num1, num2-num1)
	
def GCD(num1,num2):
	minNum = min(num1,num2)
	for i in range(minNum, 1, -1):
		if (num1 % i == 0 and num2 % i == 0):
			return i
	return 1
def main():
	num1,num2=input("Enter 2 numbers to identify GCD : ")
	print("GCD of {} and {} is {} ".format(num1,num2,GCD(num1,num2)))
	print("GCD of {} and {} is {} ".format(num1,num2,GCD1(num1,num2)))
		
if __name__=="__main__":
	main()
