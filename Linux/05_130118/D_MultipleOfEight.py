#!/usr/bin/python27
def MultipleOfEight(num):
	'''if (num & 7):
		return False
	else:
		return True'''
	return num&7==0
		
def main():
	num = input("Enter no to check if multiple of 8 : ")
	res = "Not "
	if (MultipleOfEight(num)):
		res = ""
	print("Number {} is {}multiple of 8".format(num,res))
	
if __name__=='__main__':
	main()
