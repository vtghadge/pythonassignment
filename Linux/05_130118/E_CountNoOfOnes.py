#!/usr/bin/python27
def CountOnBits(num):
	if (num < 0):
		num = num * -1
	temp = 1
	count = 0
	while(temp <= num):
		if num & temp:
			count+=1
		temp = temp<<1
	return count
def main():
	num= input("Enter number and count 1 bits in it: ")
	print("No of 1's in number {} is {} ".format(num,CountOnBits(num)))
if __name__=='__main__':
	main()
