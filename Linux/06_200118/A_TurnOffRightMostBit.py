#!/usr/bin/python27
def TurnOffRightMostBit(num):
	x = 1
	count = 0
	if (num == 0):
		return 0
		
	while (num & x) == 0:
		x = x<<1
		count+=1
	return (num & (~x))#return num ^ x	

def main():
	num = input("Enter number to turn off right most bit : ")
	offBitNum = TurnOffRightMostBit(num)
	print("After turning off right most bit of number {} resulting number is {} ".format(num,offBitNum))
	
if __name__=="__main__":
	main()
