#!/usr/bin/python27
def TurnOffSetOFBits(num, position, numOfBits):
	x = (1<<numOfBits) -1
	
	if (num == 0 or numOfBits > position):
		return 0
		
	x = x<<(position-numOfBits)
		
	return (num & (~x))

def TurnOnSetOFBits(num, position, numOfBits):
	x = (1<<numOfBits) -1
	
	if (numOfBits > position):
		return 0
		
	x = x<<(position-numOfBits)
		
	return (num | x)
	
def ToggleSetOFBits(num, position, numOfBits):
	x = (1<<numOfBits) -1
	
	if (numOfBits > position):
		return 0
		
	x = x<<(position-numOfBits)
		
	return (num ^ x)

def SwapSetOFBits(num1, num2, position, numOfBits):
	x = (1<<numOfBits) -1

	if (numOfBits > position):
		return 0
	
	num1Pad = num1 & x
	num2Pad = num2 & x
	
	offnum1 = TurnOffSetOFBits(num1, position,numOfBits)
	offnum2 = TurnOffSetOFBits(num2, position,numOfBits)
	
	resnum1 = offnum1	| num2Pad
	resnum2 = offnum2	| num1Pad
	
	return resnum1,resnum2
		
def main():
	#num,numOfBits,pos = input("Enter number, and number of bits and position from which number of bits turn off/on/Toggle: ")
	#offBitNum = TurnOffSetOFBits(num,pos,numOfBits)
	#print("After turning off {} bits of number {} from position {} resulting number is {} ".format(numOfBits,num,pos,offBitNum))
	
	#num,numOfBits,pos = input("Enter number, and number of bits and position from which number of bits turn on: ")
	#onBitNum = TurnOnSetOFBits(num,pos,numOfBits)
	#print("After turning on {} bits of number {} from position {} resulting number is {} ".format(numOfBits,num,pos,onBitNum))

	#ToggleBitNum = ToggleSetOFBits(num,pos,numOfBits)
	#print("After toggling {} bits of number {} from position {} resulting number is {} ".format(numOfBits,num,pos,ToggleBitNum))

	num1,num2,numOfBits,pos = input("Enter number1, number2, and number of bits and position from which swap number of bits: ")
	resnum1,resnum2 = SwapSetOFBits(num1,num2,pos,numOfBits)
	
	print("After Swapping {} bits of number1 {} and number {} from position {} resulting number1 {} and number {} ".format(numOfBits,num1, num2,pos,resnum1,resnum2))
	
if __name__=="__main__":
	main()
