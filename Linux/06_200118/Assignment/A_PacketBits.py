#!/usr/bin/python2.7
'''
WAP to accept following values from user, CRC - 3,length 7, data 16, flag 2, Protocol type - 4
'''
def MakePacket(CRC,length,data,flag,ptype):
	packet = CRC & ((1<<3) - 1)
	packet = packet << 7
	packet |= length & ((1<<7) - 1)
	packet = packet << 16
	packet |= data & ((1<<16) - 1)
	packet = packet << 2
	packet |= flag & ((1<<2) - 1)
	packet = packet << 4
	packet |= ptype & ((1<<4) - 1)
	
	return packet
	
def DePacket(packet):
	ptype = packet & ((1<<4) - 1)
	packet = packet >> 4
	flag = packet & ((1<<2) - 1)
	packet = packet >> 2
	data = packet & ((1<<16) - 1)
	packet = packet >> 16
	length = packet & ((1<<7) - 1)
	packet = packet >> 7
	CRC = packet & ((1<<3) - 1)

	return CRC,length,data,flag,ptype
	
def main():
	CRC,length,data,flag,ptype = input("Enter CRC,length,data,flag and protocol type")
	packet = MakePacket(CRC,length,data,flag,ptype)
	print("Resulting packet is {}".format(packet))
	CRC,length,data,flag,ptype = DePacket(packet)
	print("Packet {}, CRC: {}, length: {}, data: {}, flag: {}, protocol type: {}".format(packet,CRC,length,data,flag,ptype))
	
if __name__=="__main__":
	main()
