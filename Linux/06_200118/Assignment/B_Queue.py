#!/usr/bin/python2.7
'''
WAP To implement queue using list.
	- enqueue()
	- dequeue()
		pop(0)
	- IsEmpty()
	- IsFull()
'''
def IsFull(l1):
	return len(l1)==10

def IsEmpty(l1):
	return len(l1)==0
		
def Enqueue(l1,data):

	if (IsFull(l1)):
		print "Queue is full"
		return False
	else:
		l1.append(data)	
		return True

def Dequeue(l1):
	
	if (IsEmpty(l1)):
		print "Queue is empty"
		return 0
	else:
		return l1.pop(0)
		
def main():
	ch = 1
	l1 = []
	#size = input("enter size of list") 
	while(ch != 0):
		ch = input("1.Enqueue 2.Dequeue 3.Display 0.Exit: ")
		if ch == 1:
			data = input("Enter data to insert in Queue: ")
			Enqueue(l1,data)
		elif ch == 2:
			print("After Dequeue operation data is : {}".format(Dequeue(l1)))
		elif ch==3:
			print l1
		elif ch == 0:
			return
		else:
			continue
	
if __name__=="__main__":
	main()
