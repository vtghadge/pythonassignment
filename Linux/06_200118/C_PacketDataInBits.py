#!/usr/bin/python27
'''
 WAP to accept following values from user, CRC,length,data,flag where CRC is 5 bits, length is 8 bits, and data is 18 bits, flag is 1 bit.
'''
def MakePacket(CRC,length,data,flag):
	CRC = CRC & ((1<<5)-1)	# 5 is CRC length
	length = length & ((1<<8) - 1)
	data = data & ((1<<18)-1)
	flag = flag & 1
	
	packet = CRC;
	packet = (packet << 8) | length
	packet = (packet << 18) | data
	packet = (packet << 1) | flag
	
	return packet

def DePacketData(Packet):

	flag = Packet & 1
	Packet = Packet >> 1 

	data = Packet & ((1<<18)-1)
	Packet = Packet >> 18

	length = Packet & ((1<<8)-1)
	Packet = Packet >> 8
	
	CRC = Packet & ((1<<5)-1)
	
	return CRC,length,data,flag

def main():

	CRC,length,data,flag = input(" Enter CRC,length,data and flag and create packet from it : ")
	
	Packet = MakePacket(CRC,length,data,flag)
	
	print("Packet is {}".format(Packet))
	
	CRC,length,data,flag = DePacketData(Packet)
	
	print("Original Packet CRC {}, length {}, data {}, flag {} ".format(CRC,length,data,flag))
	
	
if __name__=="__main__":
	main()

