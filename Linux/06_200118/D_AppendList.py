#!/usr/bin/python27
'''
WAP which accept a list and an element to be appended to the list , if the element to be appended is container then list should be appended other wise it should append.
'''
def appendEx(l1, data2):

	if type(data2)== list:
		l1.extend(data2)
	else:
		l1.append(data2)

def main():
	
	l1 = [1,2,3,4]
	data = 5
	appendEx(l1,data)
	print l1
	data = [6,7,8]
	appendEx(l1,data)
	print l1
	
if __name__=="__main__":
	main()
