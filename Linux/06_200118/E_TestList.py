import copy
def main():
	l1 = [1,2,3,[4,5,6],7,8]
	print id(l1)
	print id(l1[3])
	print l1
	
	l2= copy.copy(l1)

	print id(l2)
	print id(l2[3])
	print l2
	
	l3 = copy.deepcopy(l1)
	print id(l3)
	print id(l3[3])
	print l3
	
if __name__=="__main__":
	main()
 
