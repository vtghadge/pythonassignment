'''
WAP to implement a stack using list.
'''
def IsFull(l1):
		return len(l1)==10

def IsEmpty(l1):
	return len(l1)==0
		
def pushEx(l1,data):

	if (IsFull(l1)):
		print "stack is full"
		return False
	else:
		l1.append(data)	
		return True

def popEx(l1):
	
	if (IsEmpty(l1)):
		print "Stack is empty"
		return 0
	else:
		return l1.pop()
		
def main():
	ch = 1
	l1 = []
	#size = input("enter size of list") 
	while(ch != 0):
		ch = input("1.Push 2.Pop 3.Display 0.Exit")
		if ch == 1:
			data = input("Enter data to insert in stack: ")
			pushEx(l1,data)
		elif ch == 2:
			print("After pop operation data is : {}".format(popEx(l1)))
		elif ch==3:
			print l1
		elif ch == 0:
			return
		else:
			continue
			
if __name__=="__main__":
	main()
 
