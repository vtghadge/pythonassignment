#!/usr/bin/python2.7
'''
L1 = [5,3,9,2,1]
L2 = [8,4,5,10,2,7]
WAP to accept a 2 list from user, sort them without using built in sort method and then merge the 2 sorted list.
'''
def Merge(L1,L2):
	L3 = []
	len1 = len(L1)
	len2 = len(L2)

	i=j=0	
	while i < len1 and j < len2:
		if (L1[i]>L2[j]):
			L3.append(L2[j])
			j += 1
		else:
			L3.append(L1[i])
			i += 1
	else:
		if (i == len1):
			L3.extend(L2[j:])
		else:
			L3.extend(L1[i:])
	return L3
	
def SortEx(L1):
	for i in range(len(L1)):
		for j in range(i+1,len(L1)):
			if L1[i] > L1[j]:
				temp = L1[i]
				L1[i]= L1[j]
				L1[j]=temp
			
def main():

	L1 = input("Enter L1: ")
	L1 = list(L1)
	SortEx(L1)
	print L1
	
	L2 = input("Enter L2: ")
	L2 = list(L2)
	SortEx(L2)
	print L2

	L3 = Merge(L1,L2)
	print("after merging , Resulting List {} ".format(L3))
		
if __name__=="__main__":
	main()
