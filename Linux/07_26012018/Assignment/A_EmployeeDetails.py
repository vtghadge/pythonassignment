#!/usr/bin/python27
'''
WAP to accept employee details from user and perform following operations on the same 
	- search employee
	- modify employe
	- update deatails
	- display
	- insert
	- remove an employee
	details - name,DOB,address,salary.
	Menu driven :
		ch = menu()
		while ch!=0
			'
			'
			'
			ch Menu()
'''
g_ID = 0
def IsEmployeePresent(dictEmployeeDetails, name):
	return dictEmployeeDetails.has_key(name)
	
def RemoveEmployee(dictEmployeeDetails, name):
	if (False == IsEmployeePresent(dictEmployeeDetails, name)):
		print("Employee {} is not present ".format(name))
		return
		
	dictEmployeeDetails.pop(name)
	
def AddEmployeeDetails(dictEmployeeDetails):
	global g_ID
	dictDetails = {}
	
	name = input("Enter Employee Name: ")
	address = input("Enter Employee Address: ")
	DOB = input("Enter Employee DOB: ")
	salary = input("Enter Employee Salary: ")
	
	#dictDetails['name'] = name
	dictDetails['address'] = address
	dictDetails['DOB'] = DOB
	dictDetails['salary'] = salary
	dictDetails['ID'] = g_ID + 1
	dictEmployeeDetails[name] = dictDetails
	
def UpdateEmployeeDetails(dictEmployeeDetails,name):
	if (False == IsEmployeePresent(dictEmployeeDetails, name)):
		print("Employee {} is not present ".format(name))
		return
	dictDetails = dictEmployeeDetails[name]

	address = input("Enter Employee Address: ")
	DOB = input("Enter Employee DOB: ")
	salary = input("Enter Employee Salary: ")
	
	dictDetails['address'] = address
	dictDetails['DOB'] = DOB
	dictDetails['salary'] = salary
	dictEmployeeDetails[name] = dictDetails

def Display(dictEmployeeDetails):
	if len(dictEmployeeDetails) == 0:
		print("Employee details not present....")
		return
		
	for key,val in dictEmployeeDetails.items():
		print("")
		print("Employee name : {}".format(key))
		subDict = dictEmployeeDetails[key]
		for key1,val1 in subDict.items():
			print("Employee {} : {}".format(key1,val1))
			
def Menu():
	print("1.Insert Employee 2.Serch Employee 3.Modify Employee 4.Remove Employee 5.Display 0.Exit")
	choice = input("Enter choice: ")
	return choice
	
def main():
	dictEmployeeDetails = {}
	Id = 1
	choice = Menu()
	while(choice != 0):
		if (1 == choice):
			AddEmployeeDetails(dictEmployeeDetails)
			
		elif(2 == choice):
			name = input("Enter employee name to search : ")
			strRes = "Not "
			if (IsEmployeePresent(dictEmployeeDetails, name)):
				strRes = ""
			print("Employee {} is {}present".format(name,strRes))
			
		elif(3 == choice):
			name = input("Enter employee name to update details : ")
			UpdateEmployeeDetails(dictEmployeeDetails,name)
			
		elif(4 == choice):
			name = input("Enter employee name to remove : ")
			RemoveEmployee(dictEmployeeDetails, name)
			
		elif(5 == choice):
			Display(dictEmployeeDetails)
		else:
			print("Invalid Choice:")
			
		choice = Menu()
		
if __name__=="__main__":
	main()
