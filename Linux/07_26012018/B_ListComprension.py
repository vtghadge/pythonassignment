#!/usr/bin/python2.7
'''
Write a list comprehension to return unique elements list.
'''
def Table(x):
	print([x*i for i in range(1,11)])
	'''
	for i in range(1,10):
		print x*i
	return True
	'''

def TableInRange(lb,ub):
	print([[x*i for i in range(1,11)] for x in range(lb,ub)])

def isUnique(l1,element):
	return l1.count(element) == 1 or l1.remove(element)==None
	
def uniqueElements(l1):
	print([x for x in l1 if isUnique(l1,x)])# or l1.remove(x)==None])
	
def Intersection(l1,l2):
	print [x for x in l1 if x in l2]
def main():
	'''
	l1 = input("Enter list:")
	uniqueElements(l1)
	'''
	l1 = input("Enter list 1: ")
	l2 = input("Enter list 2: ")
	Intersection(l1,l2)
	
if __name__=="__main__":
	main()
