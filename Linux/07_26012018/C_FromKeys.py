#!/usr/bin/python2.7
'''
Implement from keys such that 
	
	>>> print x
	{2: 3, 'hi': 5, 'hello': 10}
	>>> y = dict.fromkeys(x)
	>>> print y
	{2: None, 'hi': None, 'hello': None}
	>>> y = dict.fromkeys(x,[7,8,9])
	>>> print y
	{2: [7, 8, 9], 'hi': [7, 8, 9], 'hello': [7, 8, 9]}
	>>> 
	
	Implement as 
		{2: 7, 'hi': 8, 'hello': 9}
		Ignore if there is more data than keys
'''

def FromKeys(input_dict,dict_val):
	result_dict = {}
	
	if type(dict_val) == list or type(dict_val) == tuple:
		keys_list = input_dict.keys()
		values_len = len(dict_val)
		for i in range(len(keys_list)):
			if i < values_len:
				result_dict[keys_list[i]] = dict_val[i]
			else:
				result_dict[keys_list[i]] = None
		else:
			result_dict=dict.fromkeys(input_dict,dict_val)
			
	return result_dict
	
'''
def fromkeysEx(d1,l1):
	len1 = len(l1)
	i = 0
	for key,val in d1.items():
		if len1 == 1:
			d1[key] = l1[i]
		else:
			d1[key] = l1[i]
			i = i+1
'''		
def main():
	'''
	l1 = input("Enter list:")
	uniqueElements(l1)
	'''
	d1 = input("Enter Dictionary: ")
	l1 = input("Enter data for keys: ")

	print FromKeys(d1,l1)
	
if __name__=="__main__":
	main()
