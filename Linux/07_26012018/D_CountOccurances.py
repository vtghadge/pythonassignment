#!/usr/bin/python2.7
'''
- WAP 
	i/p- aabbcccddabbb
	o/p - a2b2c3d2a1b3										

'''
def DuplicateStrToDictWithIndex(instr):
	resstr = ""
	count = 1
	temp = ""

	resdict = {}
	for i in range(len(instr)):
		if temp == instr[i]:
			continue
		temp = instr[i]
		count = 0
		start = i
		subdict = {}
		while i < len(instr) and instr[i] == temp:
			i+=1
			count+=1
		resstr+= temp + str(count)
		subdict[start] = i-1
		if resdict.__contains__(temp):
			tempdict = resdict[temp]
			tempdict[start] = i-1
			resdict[temp] = tempdict
		else:
			resdict[temp] = subdict
		
	return resdict
	
def CountOccurance(instr):
	resstr = ""
	count = 1
	temp = ""
	
	for i in range(len(instr)):
		if temp == instr[i]:
			continue
		temp = instr[i]
		count = 0
		while i < len(instr) and instr[i] == temp:
			i+=1
			count+=1
		resstr+= temp + str(count)
	return resstr

def DuplicateStrToDict(instr):
	d1 = {}
	for i in range(0,len(instr),2):
		d1[instr[i]] = instr[i+1]
	return d1

def main():
	instr = input("Enter string: ")
	resstr = CountOccurance(instr)
	print resstr
	d1 = DuplicateStrToDict(resstr)
	
	print d1
	
	d1 = DuplicateStrToDictWithIndex(instr)
	print d1
	
if __name__=="__main__":
	main()
