#!/usr/bin/python27
def IsFilterApplicable(line,Filter):
	line = line.rstrip()
	line = line.lower()
	Filter = Filter.lower()
	if Filter == "":
		return True
	elif Filter.startswith("^"):
		return line.startswith(Filter[1:])
	elif Filter.endswith("$"):
		return line.endswith(Filter[:-1])
	else:
		return Filter in line
			 
def CountLinesByFilter(inFile, Filter):
	fd = open(inFile)
	Count = 0
	line = fd.readline()
	while(line != ""):
		#print line
		if IsFilterApplicable(line,Filter):
			Count+=1
		line = fd.readline()
		
	fd.close()
	return Count
	
def main():
	inFile = input("Enter name of file: ")
	Filter = input("Enter filter(empty,^Hello,Hello$,Hello) to apply on this file to count lines: ")
	Count = CountLinesByFilter(inFile, Filter)
	print("Line Count {} by applying Filter {} on file {} ".format(Count,Filter,inFile))
	
if __name__=="__main__":
	main()
