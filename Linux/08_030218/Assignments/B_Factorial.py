def Factorial(num):
	fact = -1
	if num > 0:
		if num < 3:
			fact=num
		else:
			fact = 1
			while(num!=1):
				fact = fact * num
				num -=1
	return fact
'''
Direct recursion:
	def A()
		A()
Indirect recursion:
	def A()
		B()
	def B()
		A()'''
'''
------------------------------
num = 0 	return 1			Stack frame of Fact(0)
------------------------------
num = 1		return 1 * Fact(0)	Stack frame of Fact(1)
------------------------------
num = 2		return 2 * Fact(1)	Stack frame of Fact(2)
------------------------------
num = 3		return 3 * Fact(2)	Stack frame of Fact(3)
------------------------------
num = 4		return 4 * Fact(3)	Stack frame of Fact(4)
------------------------------
num = 5		return 5 * Fact(4)	Stack frame of Fact(5)
------------------------------
'''
def FactorialRec(num):
	if num < 0:
		num *= -1 
	if num == 0:
		return 1
	return num * FactorialRec(num-1)
	
def main():
	num=input("enter number to print factorial of it: ")
	print("Factorial using recursion of number {} is {} ".format(num,FactorialRec(num)))
	print("Factorial of number {} is {} ".format(num,Factorial(num)))
	
if __name__=="__main__":
	main()
