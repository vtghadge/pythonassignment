#!/usr/bin/python27
def GetLongestLine(inFile):
	longestlen = 0
	longestLine = ""
	fd = open(inFile)
	line = fd.readline()
	while line != "":
		if len(line) > longestlen:
			longestLine = line
			longestlen = len(line)
		line = fd.readline()
	
	return longestLine
		
def main():
	inFile = input("Enter name of file: ")
	LongestLine = GetLongestLine(inFile)
	print("Longest line from file {} is *{}* ".format(inFile,LongestLine))
	
if __name__=="__main__":
	main()
