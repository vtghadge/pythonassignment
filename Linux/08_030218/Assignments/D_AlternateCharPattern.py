#!/usr/bin/python27
def AlternateChars(inFile,skipChar):
	fd = open(inFile)
	buf = fd.read(skipChar)
	while (buf != ""):
		print buf,
		fd.seek(skipChar,1)
		buf = fd.read(skipChar)

	print "printing skip chars--->"
	
	fd.seek(skipChar,0)
	buf = fd.read(skipChar)
	while (buf != ""):
		print buf,
		fd.seek(skipChar,1)
		buf = fd.read(skipChar)
	
def main():
	inFile = input("Enter name of file: ")
	skipChar = input("Enter no of characters to skip: ")
	AlternateChars(inFile,skipChar)
if __name__=="__main__":
	main()
