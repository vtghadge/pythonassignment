#!/usr/bin/python27
g_fd = 0
def printChars(buf):
	global g_fd
	if buf == "\n" or buf == "":
		return
	ch = g_fd.read(1)
	printChars(ch)
	print buf,

def main():
	global g_fd
	inFile = input("Enter name of file: ")
	g_fd = open(inFile)
	ch = g_fd.read(1)
	while (ch != ""):
		printChars(ch)
		ch = g_fd.read(1)
		print ""
		
	#print ch,
	
if __name__=="__main__":
	main()
