#!/usr/bin/python27
from B_Factorial import FactorialRec
def IsSpecialNum(Num):
	tempNum = Num
	final = 0
	while (tempNum > 0):
		rem = tempNum % 10
		final += FactorialRec(rem)
		tempNum = tempNum/10
	return (final == Num)
	
def main():
	Num = input("Enter Number to check for special number: ")
	res = "Not "
	if IsSpecialNum(Num):
		res = ""
	print("Number {} is {}special number. ".format(Num,res))
	
if __name__=="__main__":
	main()
