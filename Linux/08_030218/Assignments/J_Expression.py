#!/usr/bin/python27
'''
10. WAP which compute value of following expression
	a+aa+aaa+aaaa+..............
	2+22
	3+33+333
	4+44+444+4444
'''
def EvalulateExpression(Num, i):
	
	if (Num < i):
		return 0
		
	return (Num * ((pow(10,i) - 1)/(10-1)) + EvalulateExpression(Num,i+1))
	
def main():
	Num = input("Enter Number to evalulate expression: ")
	res = EvalulateExpression(Num,1)
	print("Number {} and its evaluated expression is {}. ".format(Num,res))
	
if __name__=="__main__":
	main()
