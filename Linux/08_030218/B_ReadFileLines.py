#!/usr/bin/python27
def ReadFileEx(inFileName):
	fd = open(inFileName)
	lines = fd.readlines()
	for line in lines:
		print line
		
	fd.close()
	
def ReadFile(inFileName):
	fd = open(inFileName)
	line = fd.readline()
	while (line != ''):
		print line
		line = fd.readline() 
		
	fd.close()
	
def main():
	inFile = input("Enter name of file: ")
	ReadFileEx(inFile)
	
if __name__=="__main__":
	main()
