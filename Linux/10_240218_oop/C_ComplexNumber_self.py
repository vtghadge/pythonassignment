# 3 + 2i
'''
__repr__ to overload print obj method
'''
class Complex(object):
	def __init__(self, real=0,imag=0):
		self.real = real
		self.imag = imag
		
	def __add__(self, number):
		result = Complex()
		if isinstance(number, Complex):
			result.real = self.real + number.real
			result.imag = self.imag + number.imag
			return result
		elif isinstance(number,int):
			result.real = self.real + number
			return result			
		else
			print("invalid parameter")
			return 0
	def __repr__(self):
		return str(self.real) + self(self.imag) + "i"

def main():
	c1 = Complex()
	print c1 

if __name__=="__main__":
	main()
