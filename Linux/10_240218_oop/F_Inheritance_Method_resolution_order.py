#!/usr/bin/python2.7
class Base(object):
	def foo(self):
		print "Base : foo"
	def bar(self):
		print "Base : bar"
		
class Derived1(Base):
	def bar(self):
		print "Derived1 : bar"

class Derived2(Base):
	def foo(self):
		print "Derived2 : foo"

class Child(Derived1, Derived2):
	pass

def main():
	obj = Child()
	obj.foo()
	obj.bar()
	
if __name__=="__main__":
	main()
