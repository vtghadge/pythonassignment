'''
1)WAP to validate E-mail ID using regular expressions
2)WAP to accept an alphanumeric string from user and remove all characters other than digits
	using regular expressions
'''
import re
def CompileExp():
	expr = "([\w\.]+)@([\w\.]+)\.(\w+)"

	l = ["vtghadge@gmail.com", "v@g@gmail.com", "abc@gmail","vishal.ghadge@gmail.com"]
	expObj = re.compile(expr);
	
	for mail in l:
		res = expObj.search(mail)
		
		if (None != res and 0 == res.start() and len(mail)== res.end()):
			print ("{} match".format(mail))
		else:
			print ("{} not match".format(mail))
	
def verifyemail(szEmail):
	#regExpr = re.search("[a-zA-Z0-9_\.]+@{1}[a-zA-Z]+\.{1}[a-zA-Z]+", szEmail)
	exp = "([\w\.]+)@([\w\.]+)\.(\w+)"
	print exp
	
	regExpr = re.search(exp, szEmail)
	if None == regExpr:
		return False
	if 0 == regExpr.start() and len(szEmail) == regExpr.end():
		return True
	
	return False

def rmCharFromString(szStr):
	x = re.findall("[a-zA-Z]+", szStr)
	i = 0
	while i < len(x):
		szStr = re.sub(x[i], "", szStr, 1)
		i += 1
	print("Removed all characters from string:{}".format(szStr))

def GetNumbers(szStr):
	obj = re.compile("[0-9]+")
	x = obj.findall(szStr)
	print x

def main():
	'''
	szEmailID = input("Enter e-mail ID:")
	bRet = verifyemail(szEmailID)
	if True == bRet:
		print("Correct Email-ID")
	else:
		print("Incorrect Email-ID")
		
	rmCharFromString(szEmailID)
	'''
	'''
	CompileExp()
	'''
	szStr = input("Input String : ")
	GetNumbers(szStr)
	
if __name__ == '__main__':
		main()
