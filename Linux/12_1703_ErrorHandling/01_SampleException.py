import sys
def TestDivision(numerator,denominator):
	try:
		result = numerator/denominator
		print result
	except ZeroDivisionError as e:
		print "ZeroDivisionError ", e
		sys.exit(0)
	except ArithmeticError as e: 
		print "ArithmeticError ", e
		sys.exit(0)
	except BaseException as e: 
		print "BaseException ",e
	else:
		print "No exception occured"
	finally:
		print "Executing in any case"
		
def main():
	TestDivision(10,20)
	TestDivision(10,0)
	
if __name__=="__main__":
	main()

