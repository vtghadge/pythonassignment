import subprocess
import time

def DisplayRunningProcess():
	fd = open("procStat.txt", "a")
	x = subprocess.check_call("ps -A", shell=True, stdout=fd)
	fd.close()
	#print x
	
def main():
	in_time = input("Enter time interval: ")
	while(True):
		DisplayRunningProcess()
		time.sleep(in_time)
		
if __name__=="__main__":
	main()
