import threading
import time

def worker(name):
	time.sleep(3)
	print("{} Worker Finished".format(name))
	print("Thread name : {}".format(threading.currentThread().getName()))

def main():
	t1 = threading.Thread(target=worker, args=("Vishal",), name = "Worker Thread")
	t1.start()
	t1.join()
	
	for i in range(1,6):
		print i
	
if __name__=="__main__":
	main()
	
'''
Without Join:

1
2
3
4
5
Worker Finished

With Join

Vishal Worker Finished
Thread name : Worker Thread
1
2
3
4
5

'''
