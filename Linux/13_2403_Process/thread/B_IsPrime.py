#!/usr/bin/python2.7
import math
def isPrime(num):
	bFlag= False
	if num < 0:
		num=num*-1
	if (num % 2 == 0):
		return bFlag
	x = 3
	for x in range(3,(int)(math.sqrt(num))+1, 2):
		if (num % x == 0):
			break
	else
		bFlag = True #It will execute only when x reaches end of loop(not break in between loop). 
		
	return bFlag
def main():
	#give input like this: enter start range to check for odd numbers :1,10
	start_range,end_range = input("enter start range to check for prime numbers :")	

	if start_range > end_range:
		print("invalid start {} and end {} range".format(start_range,end_range))
		return
	x = start_range
	for x in range(start_range,end_range+1):
		if isPrime(x):
			print("Prime {}".format(x))
	#	else:
	#		print("even number {}".format(x))
if __name__=='__main__':
	main()
