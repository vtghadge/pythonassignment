from threading import Thread
from threading import Lock
import time

threads = []
data = 0
lock = Lock()

'''
Problem 1: Lack of syncronization
def get_job():
	global data

	data+=1
	time.sleep(0.1)
	return data

'''
'''
Problem 2: Still chance of race condition after lock release and befor returning data another thread gets schedule. 
def get_job():
	global data
	lock.acquire()
	data+=1
	time.sleep(0.1)
	lock.release()
	return data	
'''

def get_job():
	global data
	lock.acquire()
	data+=1
	time.sleep(0.1)
	x = data
	lock.release()
	return x
	
def process_job():
	print get_job()
	
def main():
	for i in range(10):
		thread = Thread(target=process_job)
		threads.append(thread)
		thread.start()
		
	for thread in threads:
		thread.join()

if __name__=="__main__":
	main()
