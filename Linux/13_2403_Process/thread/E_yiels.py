'''
Yield: Generating data dynamically
Return in between function
when call again it start from previous state

Iterator: Data is aleardy present.

'''

def square(n):
	for i in range(1,n):
		yield i*i

def yieldEx():		
	x = square(5)
	print next(x)
	print next(x)
	print next(x)
	print next(x)

def iterEx():
	x = [1,2,3,4,5,6]
	y = iter(x)
	print next(y)
	print next(y)
	print next(y)
	print next(y)
	print next(y)
	
def main():
	print("Yield: ")
	yieldEx()
	print("Iterator: ")
	iterEx()
	
if __name__=="__main__":
	main()
