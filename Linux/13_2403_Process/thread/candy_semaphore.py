#!/usr/bin/env python
from random import randrange
from threading import BoundedSemaphore, Lock, Thread
from time import sleep

lock = Lock()
MAX = 10
candytray = BoundedSemaphore(MAX)
def refill():
	lock.acquire()
	print 'Refilling candy...',
        print candytray._Semaphore__value
	try:
		candytray.release()
        except ValueError:
	        print 'full, skipping'
	else:
		print 'OK'
	lock.release()

def buy():
         lock.acquire()
         print 'Buying candy...',
         print candytray._Semaphore__value
         if candytray.acquire(False):
             print 'OK'
         else:
             print 'empty, skipping'
         lock.release()
 
def producer(loops):
         for i in xrange(loops):
             #while True:
             refill()
             sleep(randrange(5))
  
def consumer(loops):
#         print loops, type(xrange(loops))
         for i in xrange(loops):
#         while True:
#             print type(i)
             buy()
             sleep(randrange(5))
  
def main():
         loops = 5
         print loops
         print 'THE CANDY MACHINE (full with %d bars)!' % MAX
         Thread(target=consumer, args=(loops+11,)).start()
         Thread(target=producer, args=(loops,)).start()
 
if __name__ == '__main__':
         main()
